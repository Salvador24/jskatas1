console.log("Display the numbers from 1 to 20")
for (let counter = 1; counter <= 20; counter++) {
    console.log(counter)
}

console.log("Display the even numbers from 1 to 20")
for (let counter = 1; counter <= 20; counter++) {
    if ((counter % 2) == 0)
        console.log(counter)
}

console.log("Display the odd numbers from 1 to 20")
for (let counter = 1; counter <= 20; counter++) {
    if ((counter % 2) == 1)
        console.log(counter)
}

console.log("Display the multiples of 5 up to 100")
for (let counter = 0; counter <= 100; counter++) {
    if ((counter % 5) == 0)
        console.log(counter)
}

console.log("Display the square numbers up to 100")
for (let counter = 1; counter * counter <= 100; counter++) {

    console.log(counter * counter)
}

console.log("Display the numbers counting backwards from 20 to 1")
for (let counter = 20; counter >= 1; counter--) {
    console.log(counter)
}

console.log("Display the even numbers counting backwards from 20")
for (let counter = 20; counter >= 1; counter--) {
    if ((counter % 2) == 0)
        console.log(counter)
}

console.log("Display the odd numbers from 20 to 1 counting backwards")
for (let counter = 20; counter >= 1; counter--) {
    if ((counter % 2) == 1)
        console.log(counter)
}

console.log("Display the multiples of 5, counting down from 100")
for (let counter = 100; counter >= 0; counter--) {
    if ((counter % 5) == 0)
        console.log(counter)
}

console.log("Display the square numbers counting down from 100")
for (let counter = 100; counter >= 1; counter--) {
    if (counter * counter <= 100) {
        console.log(counter * counter)
    }
}